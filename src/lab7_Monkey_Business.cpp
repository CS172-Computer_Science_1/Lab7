#include <iostream>
#include <iomanip>
using namespace std;

const int rows = 3;
const int cols = 5;

void err(int);
void mega(int[rows][cols], double&, double&, double&);

int main()
{
	cout << "\t\t\t<<< Monkey Food Tracker >>>\n";
	char again = 'y';
	while (again == 'y' || again == 'Y'){
		int megaRay[rows][cols], meat = 0;

		//Input Block
		for (int r = 0; r < rows; r++){
			cout << "\n\t\t\t ## Monkey " << r + 1 << " Food Eaten ##\n";
			for (int c = 0; c < cols; c++){
				cout << "Day " << c + 1 << ": ";
				cin >> meat;

				//check meat input for letters and negatives
				while (cin.fail() || meat < 1){
					err(meat);
					cout << "Day " << c + 1 << ": ";
					cin >> meat;
				}
				megaRay[r][c] = meat; //assign meat to array position
			}
		}

		double avgm = 0, least = 0, greatest = 0;
		mega(megaRay, avgm, least, greatest);
		cout << "\nAverage amount of food eaten: " << avgm << endl
			<< "The least amount of food eaten: " << least << endl
			<< "The greatest amount of food eaten: " << greatest << endl;

		cout << "\n\t\tEnter y or Y to go again: ";
		cin >> again;
	}
}

void err(int meatIn){ //fix letter or negative number
	if (cin.fail())
		cout << "\t\t/!\\ Not a number /!\\\n";
	else if (meatIn < 1)
		cout << "\t\t/!\\ Negative number /!\\\n";
	cin.clear();
	fflush(stdin);
}

void mega(int mRay[rows][cols], double& avgOut, double& min, double& max){ //number of days is scalable 
	double m1 = 0, m2 = 0, m3 = 0;
	for (int i = 0; i < rows; i++){
		if (i == 0){
			for (int j = 0; j < cols; j++)
				m1 += mRay[i][j];
		}
		else if (i == 1){
			for (int k = 0; k < cols; k++)
				m2 += mRay[i][k];
		}
		else if (i == 2){
			for (int l = 0; l < cols; l++)
				m3 += mRay[i][l];
		}
	}
	avgOut = (m1 + m2 + m3) / (cols * 3);

	//min
	if (m1 < m2 && m1 < m3)
		min = m1;
	else if (m2 < m1 && m2 < m3)
		min = m2;
	else if (m3 < m1 && m3 < m2)
		min = m3;

	//max
	if (m1 > m2 && m1 > m3)
		max = m1;
	else if (m2 > m1 && m2 > m3)
		max = m2;
	else if (m3 > m1 && m3 > m2)
		max = m3;
}