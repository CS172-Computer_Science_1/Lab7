#include <iostream>
#include <random>
using namespace std;

void err(int);

int main(){
	char again = 'y';
	const int size = 15;
	int n = 0, arrayN[size];

	while (again == 'y' || again == 'Y'){
		cout << "\nEnter a number: ";
		cin >> n;

		while (cin.fail() || n < 1 || n > 100){
			err(n);
			cout << "Enter a number: ";
			cin >> n;
		}

		mt19937 prng{ random_device{} () }; //garanteed random
		uniform_int_distribution<> dist(1, 100); //range

		cout << "\nRandom numbers greater than " << n << " are: ";
		for (int i = 0; i < size; i++){
			arrayN[i] = dist(prng);
			if (arrayN[i] > n)
				cout << arrayN[i] << " ";
		}
		
		cout << "\n\nDrawn numbers: "; //show drawn numbers
		for (int j = 0; j < size; j++)
			cout << arrayN[j] << " ";

		cout << "\n\n\t\t ### Enter Y or y to repeat: ";
		cin >> again;
	}
}

void err(int nr){ //fix letter, <1 and >100
	if (cin.fail())
		cout << "\t\t/!\\ Not a number /!\\\n";
	else if (nr < 1)
		cout << "\t\t/!\\ Negative number /!\\\n";
	else if (nr > 100)
		cout << "\t\t/!\\ Greater than 100 /!\\\n";
	cin.clear();
	fflush(stdin);
}