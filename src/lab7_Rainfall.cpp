#include <iostream>
#include <iomanip>
using namespace std;

void err(double);
void yrRain(double [], const int, double&);
void avgRain(double[], const int, double&);
void hlMonths(double [], const int, double&, double&, int&, int&);

int main(){
	const int months = 12;
	char again = 'y';

	while (again == 'y' || again == 'Y'){
		double arrain[months];
		cout << "\t\tEnter the rainfall amount for 12 months.\n\n";

		for (int i = 0; i < months; i++){
			cout << "Month " << i + 1 <<": ";
			cin >> arrain[i];

			while (cin.fail() || arrain[i] < 1){ //fix letter and negatives
				err(arrain[i]);
				cout << "Month " << i + 1 << ": ";
				cin >> arrain[i];
			}
		}

		double ttl = 0;
		yrRain(arrain, months, ttl); //yearly rainfall
		cout << "\n\nThe yearly amount of rainfall was: " << setprecision(2) << fixed << ttl << endl;

		double avgIn = 0;
		avgRain(arrain, months, avgIn); //monthly rainfall
		cout << "The Average rainfall per month was: " << setprecision(2) << fixed << avgIn << endl;

		double maxR = 0, minR = 0;
		int maxM = 0, minM = 0;
		hlMonths(arrain, months, maxR, minR, minM, maxM); //months with highest and lowest rainfall
		cout << "Month " << (minM+1) << " had the lowest rainfall, and it was: " << setprecision(2) << fixed << minR << endl
			<< "Month " << (maxM+1) << " had the highest rainfall, and it was: " << setprecision(2) << fixed << maxR << endl << endl;
		
		cout << "\nEnter y/Y to go again: ";
		cin >> again;
	}
}

void err(double x){ //fix letter or negative number
	if (cin.fail())
		cout << "\t\t/!\\ Not a number /!\\\n";
	else if (x < 1)
		cout << "\t\t/!\\ Negative number /!\\\n";
	cin.clear();
	fflush(stdin);
}

void yrRain(double rainray[], const int m, double &sum){
	for (int i = 0; i < m; i++)
		sum += rainray[i];	
}

void avgRain(double rainray[], const int m, double & avgOut){
	for (int j = 0; j < m; j++)
		avgOut += rainray[j];
	avgOut /= m;
}

void hlMonths(double rainray[], const int m, double& maxRain, double& minRain, int& minMonth, int& maxMonth){
	maxRain = rainray[0];
	minRain = rainray[0];
	double tempRain = 0;

	for (int k = 0; k < m; k++){
		tempRain = rainray[k];

		if (tempRain < minRain){
			minRain = tempRain;
			minMonth = k;
		}

		if (tempRain > maxRain){
			maxRain = tempRain;
			maxMonth = k;
		}
	}
}